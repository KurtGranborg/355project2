var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT G.Group_ID, Group_Name, COUNT(gf.Group_ID) AS Members FROM Groups G ' +
    'LEFT JOIN Friend_Group gf on gf.Group_ID = G.Group_ID ' +
    'WHERE G.User_ID = (SELECT User_ID FROM CurrentUser) ' +
    'GROUP BY Group_Name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getByID = function(Group_ID, callback) {
    var query = 'SELECT * FROM Groups G ' +
        'LEFT JOIN Friend_Group gf on gf.Group_ID = G.Group_ID ' +
        'LEFT JOIN Users U ON gf.Friend_ID = U.User_ID ' +
        'WHERE G.Group_ID = ?';
    var queryData = [Group_ID];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.add = function (req, callback) {
    var getUser = 'select User_ID from CurrentUser';
    connection.query(getUser, function (err, result) {
        var query = 'Insert into Groups(User_ID, Group_Name) VALUES (?)';
        var values = [result[0].User_ID, req.query.Group_Name];
        connection.query(query, [values], function (err, result) {
            callback(err, result);
        });
    });
};