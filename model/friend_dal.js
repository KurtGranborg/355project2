/**
 * Created by Kurt on 4/29/17.
 */
/**
 * Created by Kurt on 4/29/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(Friends_Min, callback) {
    if(Friends_Min == null)
        Friends_Min = 0;
    var query = 'SELECT Friend_ID, First_Name, Last_Name, Username, (SELECT COUNT(Friend_ID) FROM Friends F WHERE F.User_ID = u.User_ID) AS Num_Friends FROM Friends f ' +
        'JOIN Users u on f.Friend_ID = u.User_ID ' +
        'WHERE f.User_ID = (SELECT User_ID FROM CurrentUser) ' +
        'GROUP by Username, Last_Name, First_Name ' +
        'HAVING Num_Friends > ?';
    connection.query(query, Friends_Min, function(err, result) {
        callback(err, result);
    });
};

exports.findFriend = function(Search, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser;";
    connection.query(getUser, function (err, result) {
        var query = 'select * from Users U ' +
            'WHERE (Username LIKE ? AND First_Name LIKE ? AND Last_Name LIKE ?) ' +
            'AND User_ID != ? AND User_ID IN( SELECT Friend_ID FROM Friends f WHERE f.User_ID = ?) ' +
            'AND User_ID NOT IN (SELECT Friend_ID FROM Friends_Invited FI WHERE FI.Event_ID = ?);';
        var search = ['%' + Search.Username + '%', '%' + Search.First_Name + '%', '%' + Search.Last_Name + '%', Search.Event_ID, result[0].User_ID, Search.Event_ID];
        connection.query(query, search, function (err, result) {
            callback(err, result);
        });
    });
};
exports.findFriends = function(Search, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser;";
    connection.query(getUser, function (err, result) {
         var query =  'select * from Users U ' +
        'WHERE (Username LIKE ? AND First_Name LIKE ? AND Last_Name LIKE ?) ' +
             'AND User_ID != ? AND User_ID NOT IN( SELECT Friend_ID FROM Friends f WHERE f.User_ID = ?);';
         var search = ['%'+Search.Username+'%', '%'+Search.First_Name+'%', '%'+Search.Last_Name+'%', result[0].User_ID, result[0].User_ID];
        connection.query(query, search, function(err, result) {
              callback(err, result);
         });
    });

};
exports.deleteFriend = function (Friend_ID, callback) {
    var deleteUser = 'DELETE FROM Friends WHERE Friend_ID = ? AND User_ID = (SELECT User_ID FROM CurrentUser);';
    connection.query(deleteUser, Friend_ID, function (err, result) {
        callback(err, result);
    })
};
exports.addFriends = function (args, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser";
    connection.query(getUser, function (err, result) {
            var add = "Insert into Friends (User_ID, Friend_ID) VALUES ?;"
            var values = [];
            if(Array.isArray(args.AddFriend)) {
                for (var i = 0; i < args.AddFriend.length; i++) {
                    values.push([result[0].User_ID, args.AddFriend[i]]);
                }
            }
            else {
                values.push([result[0].User_ID, args.AddFriend]);
            }
             connection.query(add, [values], function (err, result) {
                callback(err, result);
            });
    });
};
