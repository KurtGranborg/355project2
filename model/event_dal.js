/**
 * Created by Kurt on 4/29/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT Event_ID, Event_Name, Event_DateTime, Event_Location, Event_Desc FROM Events e ' +
        'WHERE e.User_ID = (SELECT User_ID FROM CurrentUser) OR Exists (SELECT * FROM Friends_Invited FI where ' +
        'FI.event_ID = e.event_ID AND FI.friend_ID = (SELECT User_ID FROM CurrentUser)) ' +
        'ORDER BY Event_DateTime asc;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getByID = function(Event_ID, callback) {
    var query = 'CALL event_getinfo (?)';
    var queryData = [Event_ID];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getComments = function(Post_ID, callback) {
    var query = 'SELECT * , P.Post_ID AS PostID, P.Content AS PostContent From Posts P ' +
        'LEFT JOIN Post_Comments PC on PC.Post_ID = P.Post_ID ' +
        'LEFT JOIN Users U ON PC.User_ID = U.User_ID ' +
        'WHERE P.Post_ID = ?';
    var queryData = [Post_ID];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.addFriends = function (args, callback) {
    var query = 'INSERT INTO Friends_Invited (Event_ID, Friend_ID) VALUES ?;';
    var queryData = []
    if(Array.isArray(args.AddFriend))
        for (var i = 0; i < args.AddFriend.length; i++) {
            queryData.push([args.Event_ID, args.AddFriend[i]]);
        }
        else{
             queryData.push([args.Event_ID, args.AddFriend]);
         }
    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.comment = function (args, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser";
    connection.query(getUser, function (err, result) {
        var query = 'INSERT INTO Post_Comments (Post_ID, User_ID, Content) VALUES (?);';
        var values = [args.Post_ID, result[0].User_ID, args.Comment];
        connection.query(query, [values], function(err, result) {
            callback(err, args);
        });
    })
};
exports.add = function (args, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser";
    connection.query(getUser, function (err, result) {
        var query = "Insert into Events (User_ID, Event_Name, Event_Location, Event_DateTime, Event_Desc) " +
            "VALUES (?);";
        var data = [result[0].User_ID, args.Event_Name, args.Event_Location, args.date, args.Event_Description];
        connection.query(query, [data], function(err, result) {
            callback(err, result.insertId);
        });
    });
};
exports.addPost = function (args, callback) {
    var getUser = "SELECT User_ID FROM CurrentUser";
    connection.query(getUser, function (err, result) {
        var query = "Insert into Posts (User_ID, Event_ID, Content) VALUES ?;";
        var data = [[result[0].User_ID, args.Event_ID, args.Post]];
        connection.query(query, [data], function (err, result) {
            callback(err, result);
        });
    });

}