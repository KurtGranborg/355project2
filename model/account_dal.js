
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);
exports.getUser = function(User_ID, callback) {
    var query = 'SELECT * from CurrentUser';
    connection.query(query, function(err, result) {
        if(result.length == 0){
            callback(err, null);
        }else {
            callback(err, result[0]);
        }
    });
};
var connection = mysql.createConnection(db.config);
exports.logout = function(User_ID, callback) {
    var query = 'DELETE FROM CurrentUser;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
exports.getAll = function(User_ID, callback) {
    var query = 'SELECT U.User_ID, U.First_Name, U.Last_Name, U.Email, U.Description, U.Username, COUNT(F.Friend_ID) AS NumFriends From Users U ' +
        'LEFT JOIN Friends F ON U.User_ID = F.User_ID '  +
        'WHERE U.User_ID = (SELECT User_ID FROM CurrentUser)' +
        'GROUP BY(U.User_ID);';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getFriends = function(User_ID, callback) {
    var query = 'SELECT *, (SELECT COUNT(Friend_ID) FROM Friends F WHERE F.User_ID = ue.User_ID) AS Num_Friends ' +
        'from user_events ue '  +
        'WHERE ue.User_ID = ?;';
    connection.query(query, User_ID, function(err, result) {
        callback(err, result);
    });
};
exports.insert = function(params, callback){
    var query = 'INSERT INTO Users (First_Name, Last_Name, Username, Email, Password) VALUES(?)';
    var account_data = [params.First_Name, params.Last_Name, params.Username, params.Email, params.Password];
    connection.query(query, [account_data], function(err, result) {
        var login = 'CALL login(?);';
        var user = result.insertId;
        connection.query(login, user, function(err, result) {
            callback(err, result);
        });
    });

};

exports.UserLogin = function(params,callback){
    var query = 'select * from Users where Username = ? AND Password = ?';
    var userData = [params.Username, params.Password];
    connection.query(query, userData, function(err, result){
        if(result.length != 0 && err == null) {
            var login = 'CALL login (?);';
            var account = result[0].User_ID;
            connection.query(login, [account], function (err, result) {
                callback(err, result);
            });
        }else{
            callback(err, result);

        }

    });
};

exports.logout = function (params, callback) {
    var query = 'DELETE FROM CurrentUser;';
    connection.query(query, function (err, result) {
        callback(err, result);
    });

};


exports.edit = function(params, callback){
    var query = 'UPDATE Users u SET Username = ?, First_Name = ?, Last_Name = ?, Email = ?, Description = ? WHERE u.User_ID = (select User_ID from CurrentUser);';
    var account_data = [params.Username, params.First_Name, params.Last_Name, params.Email, params.Description];
    connection.query(query, account_data, function(err, result) {
        callback(err, result);
    });
};