/**
 * Created by Kurt on 4/29/17.
 */

var express = require('express');
var router = express.Router();
var friend_dal = require('../model/friend_dal');
var account_dal = require('../model/account_dal');


// View All accounts
router.get('/all', function(req, res) {
    friend_dal.getAll(req.query.Friend_Min, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User) {
                res.render('friends/friendsViewAll', {'result': result, 'Event_ID': req.query.Event_ID, 'Current_User_ID': User});
            });
        }
    });

});
router.post('/delete', function(req,res){
    friend_dal.deleteFriend(req.body.Friend_ID, function (err, result) {
        if(err) {
            res.send(err);
        }
        else {
            friend_dal.getAll(req.query.Friend_Min, function(err, result){
                if(err) {
                    res.send(err);
                }
                else {
                    account_dal.getUser(req, function (err, User) {
                        res.render('friends/friendsViewAll', {'result': result, 'Event_ID': req.query.Event_ID, 'Current_User_ID': User, 'delete_success':true});
                    });
                }
            });
        }
    })
})
router.post('/addFriend', function(req, res) {
    friend_dal.findFriends(req.body, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User) {
                res.render('friends/friendSearch', {'result': result, 'Current_User_ID': User});
            });
        }
    });
});

router.post('/submitAdds', function(req, res) {
    if(req.body.AddFriend == null) {
        friend_dal.findFriends(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                account_dal.getUser(req, function (err, User) {
                    res.render('friends/friendSearch', {
                        'result': result,
                        'Current_User_ID': User,
                        'none_selected': true
                    });
                });
            }
        });
    }
    else {
        friend_dal.addFriends(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                friend_dal.getAll(req.body.User_ID, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        account_dal.getUser(req, function (err, User) {
                            res.render('friends/friendsViewAll', {
                                'result': result,
                                'Event_ID': req.body.Event_ID,
                                'Current_User_ID': User,
                                'was_successful': true
                            });
                        });
                    }
                });
            }
        });
    }
});



module.exports = router;
