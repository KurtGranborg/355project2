/**
 * Created by Kurt on 4/29/17.
 */
var express = require('express');
var router = express.Router();
var event_dal = require('../model/event_dal');
var friend_dal = require('../model/friend_dal');
var account_dal = require('../model/account_dal');

// View All accounts
router.get('/all', function(req, res) {
    event_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User){
                res.render('event/eventViewAll', { 'result':result, 'Current_User_ID':User});
            });
        }
    });

});

router.get('/', function(req, res) {
    event_dal.getByID(req.query.Event_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User) {
                res.render('event/eventViewByID', {'result': result, 'Current_User_ID': User});
            });
        }
    });

});

router.get('/post', function(req, res) {
    event_dal.getComments(req.query.Post_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User) {
                res.render('event/eventViewPost', {'result': result, 'Current_User_ID': User});
            });
        }
    });

});

router.get('/addFriend', function(req, res) {
    friend_dal.findFriend(req.query, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            result.Event_ID = req.query.Event_ID;
            account_dal.getUser(req, function (err, User) {
                res.render('event/eventAddFriend', {'result': result, 'Current_User_ID': User});
            });
        }
    });

});

router.post('/submitAdds', function(req, res) {
    if(req.body.AddFriend == null) {
        friend_dal.findFriend(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                account_dal.getUser(req, function (err, User) {
                    res.render('event/eventAddFriend', {
                        'result': result,
                        'Current_User_ID': User,
                        'none_selected': true
                    });
                });
            }
        });
    }else {
        event_dal.addFriends(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                event_dal.getByID(req.body.Event_ID, function (err, result) {
                    account_dal.getUser(req, function (err, User) {
                        res.render('event/eventViewByID', {
                            'result': result,
                            'Event_ID': req.body.Event_ID,
                            'Current_User_ID': User,
                            'Friend_Add_Successful': true
                        });
                    });
                });
            }
        });
    }
});

router.post('/commentPost', function(req, res){
    event_dal.comment(req.body, function(err, result){
        if(err){
            res.send(err);
        }else{
            event_dal.getComments(req.body.Post_ID, function(err, result){
                if(err) {
                    res.send(err);
                }
                else {
                    account_dal.getUser(req, function (err, User) {
                        res.render('event/eventViewPost', {'result': result, 'Current_User_ID': User, 'was_successful': true});
                    });
                }
            });
        }
    });
});

router.post('/create', function (req, res) {
    req.body.date = new Date(req.body.date);
    year = req.body.date.getFullYear();
    month = req.body.date.getMonth()+1;
    day = req.body.date.getDate();
    if (day < 10) {
        day = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }
        req.body.date = year+"-"+month+"-"+day+" "+req.body.time;
        event_dal.add(req.body, function (err, result) {
        if(err){
            res.send(err);
        }else {
            event_dal.getByID(result, function(err, result){
                if(err) {
                    res.send(err);
                }
                else {
                    account_dal.getUser(req, function (err, User) {
                        res.render('event/eventViewByID', {'result': result, 'Current_User_ID': User, 'was_successful': true});
                    });
                }
            });

        }
    })
});

router.post('/PostStatus', function (req, res){
    if(req.body.Post == ""){
        res.send(err);
    }else{
        event_dal.addPost(req.body, function (err, result) {
            if(err) {
                res.send(err);
            }
            else {
                event_dal.getByID(req.body.Event_ID, function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        account_dal.getUser(req, function (err, User) {
                            res.render('event/eventViewByID', {'result': result, 'Current_User_ID': User, 'post_was_successful': true});
                        });
                    }
                });
            }
        });
    }
});
module.exports = router;
