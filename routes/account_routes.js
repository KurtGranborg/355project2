/**
 * Created by Kurt on 4/29/17.
 */


var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(req.query.User_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User){
                res.render('Account/myAccount', {'result':result , 'Current_User_ID':User});
            });
        }
    });
});

router.get('/myAccount', function(req, res) {
    account_dal.getAll(req.query.User_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User){
                res.render('Account/myAccount', {'result':result , 'Current_User_ID':User});
            });
        }
});
});


router.get('/friend', function(req, res) {
    account_dal.getFriends(req.query.User_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User){
                if(User.User_ID == req.query.User_ID)
                    res.render('Account/myAccount', {'result':result , 'Current_User_ID':User});
                else
                   res.render('Account/Account', {'result':result , 'Current_User_ID':User});
                });
        }
    });
});
router.post('/insert', function (req, res) {
    if(req.body.Username == "") {
        account_dal.getUser(req, function (err, User){
            res.render( 'index', {'title':'HMU' , 'Current_User_ID':User, 'username_missing': true});
        })
    }else if(req.body.Password == "") {
        account_dal.getUser(req, function (err, User){
            res.render( 'index', {'title':'HMU' , 'Current_User_ID':User, 'password_missing': true});
        })
    }else if(req.body.Password2 != req.body.Password){
        account_dal.getUser(req, function (err, User){
            res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'password_missmatch': true});
        })
    }else if(req.body.Last_Name == "") {
        account_dal.getUser(req, function (err, User){
            res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'last_missing': true});
        })
    }else if(req.body.First_Name == "") {
        account_dal.getUser(req, function (err, User){
            res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'first_missing': true});
        })
    }
    else if(req.body.Email == "") {
        account_dal.getUser(req, function (err, User){
            res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'email_missing': true});
        })
    }else if(req.body.Email != req.body.Email2){
        account_dal.getUser(req, function (err, User){
            res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'email_missatch': true});
        })
    }
    else {
        account_dal.insert(req.body, function(err,result) {
            if (err) {
                account_dal.getUser(req, function (err, User){
                    res.render('index', {'title':'HMU' , 'Current_User_ID':User, 'unknown_issue': true});
                })
            }
            else {
                account_dal.getAll(req.query.User_ID, function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        account_dal.getUser(req, function (err, User){
                            res.render('Account/myAccount', {'result':result , 'Current_User_ID':User});
                        });
                    }
                });

            }
        });
    }
});
router.post('/login', function(req,res) {
    if (req.body.Username == "") {
    account_dal.getUser(req, function (err, User) {
        res.render('index', {'title': 'HMU', 'Current_User_ID': User, 'Username_missing': true});
    })
        }
        else if(req.body.Password == "")
        {
            account_dal.getUser(req, function (err, User) {
                res.render('index', {'title': 'HMU', 'Current_User_ID': User, 'Userpass_missing': true});
            })
        }else{
        account_dal.UserLogin(req.body, function(err,result){
            if(result.length == 0){
                account_dal.getUser(req, function (err, User) {
                    res.render('index', {'title': 'HMU', 'Current_User_ID': User, 'Userpass_missing': true});
                })            }
            else{
                account_dal.getAll(req.body.User_ID, function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        account_dal.getUser(req, function (err, User){
                            res.render('Account/myAccount', {'result':result , 'Current_User_ID':User});
                        });
                    }
                });

            }
        })
    }

} );

router.post('/edit', function(req,res){
    if(req.body.Username == null)
        res.sent('Username cannot be null');
    else if(req.body.Email == null)
        res.sent('Email cannot be null');
    else if(req.body.First_Name == null || req.body.Last_Name == null)
        res.sent('Name cannot be null');
    else {
        account_dal.edit(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                account_dal.getAll(req.body.User_ID, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        account_dal.getUser(req, function (err, User) {
                            res.render('Account/myAccount', {
                                'result': result,
                                'Current_User_ID': User,
                                'was_successful': true
                            });
                        });
                    }

                })
            }
        })
    }

});
module.exports = router;
