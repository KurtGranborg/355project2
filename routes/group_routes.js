/**
 * Created by Kurt on 4/29/17.
 */

var express = require('express');
var router = express.Router();
var group_dal = require('../model/group_dal');
var account_dal = require('../model/account_dal');


// View All accounts
router.get('/all', function(req, res) {
    group_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('groups/groupsViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res) {
    group_dal.getByID(req.query.Group_ID, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('groups/groupsViewByID', { 'result':result });
        }
    });
});

router.get('/add', function(Group_Name, res){
    group_dal.add(Group_Name, function(err, result){
        if(err){
            res.send(err);
        }else{
           res.redirect("/groups/all");
        }
    })
});


module.exports = router;
/**
 * Created by Kurt on 4/29/17.
 */
