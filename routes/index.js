var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
    account_dal.getUser(req, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('index', { title: 'HMU', 'Current_User_ID':result });
        }});
});

router.get('/about', function (req, res, next) {
    account_dal.getUser(req, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('about/about', {title: 'HMU', 'Current_User_ID':result });
        }});
});
router.get('/logout', function (req, res, next) {
    account_dal.logout(req, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            account_dal.getUser(req, function (err, User){
            res.render('index', {'title': 'HMU', 'Current_User_ID':User});
        });
        }});
});

module.exports = router;
